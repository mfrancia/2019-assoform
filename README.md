# Sistemi Informativi

Link: http://bitbucket.org/mfrancia/2019-assoform/

### Whoami?

Matteo Francia, dottorando (m.francia@unibo.it)

### Corso

ASSOFORM ROMAGNA, Rimini. Via IV Novembre, 37  Rimini 

Riferimento: Roberto Bartoli <rbartoli@assoformromagna.it >

#### Day 1 (8h) - 28/05/2019

- Introduzione ai database
- Modello relazionale, E/R
- RDBMS (MS Access)
- SQL DDL / DML

#### Day 2 (4h) - 03/06/2019

- Cenni di DB NoSQL
- RDBMS (MySQL)
- SQL DDL / DML
